.PHONY: check clean docs format image lint

BIN_NAME := prometheus-matrix

LOCAL_BINARY_PATH = $(shell stack path --local-install-root)

clean:
	stack clean

docs:
	stack haddock

package: clean
	dpkg-buildpackage -b -us -uc

package-clean:
	rm -f debian/prometheus-matrix.substvars debian/prometheus-matrix.debhelper.log

$(LOCAL_BINARY_PATH)/bin/$(BIN_NAME):
	stack build
