module Network.PrometheusMatrix.Handler (
    alertHander
  ) where


import           Network.PrometheusMatrix.Prelude hiding ((%))
import           Servant

import           Formatting

import           Network.PrometheusMatrix.Types
import           Network.PrometheusMatrix.Config
import           Network.PrometheusMatrix.JSONTypes

import           Network.Matrix

import qualified Data.Text as T
import qualified Text.Pretty.Simple as S


alertHander :: Message -> AppHandler NoContent
alertHander m = do
  conf <- ask
  S.pPrintOpt S.defaultOutputOptionsNoColor m
  void $ publish conf (formatMsg m)
  return NoContent


publish :: MonadIO m => Config -> SendMsgEventRequest -> m (Either ServantError ())
publish config msg =
  runMatrixT mConfig (printResponse =<< matrix . sendMsgEvent (matrixRoomId config) msg =<< generateTxId)
  where
    mConfig = ClientConfig {serverUri = matrixHost config, serverToken = matrixToken config}
    printResponse = either print print


formatMsg :: Message -> SendMsgEventRequest
formatMsg msg = formattedMsg "" $ sformat spec uri rec stat n (fmtAlerts alts)
  where
    spec = "<a href='" % stext % "/#/alerts?receiver=" % stext  % "'>[" % stext % ":" % int % "]</a> " % stext
    alts = alerts msg
    uri = externalURL msg
    rec = receiver msg
    stat = status msg
    n = length $ alerts msg


fmtAlerts :: [Alert] -> Text
fmtAlerts = addUl . T.concat . map (addLi . fmtAlert)
  where
    addLi e = ("<li>" :: Text) <> e
    addUl li = "<ul>" <> li <> "</ul>"


fmtAlert :: Alert -> Text
fmtAlert a = maybe (err "annotation") fmtSumm $ summary (annotations a)
  where
    fmtSumm s = (sformat $ stext % " [" % stext % "]" ) s (fmtLabel $ labels a)
    fmtLabel = fromMaybe (err "label") . job
    err t = " – ERROR: Alert " <> t <> " is empty."
