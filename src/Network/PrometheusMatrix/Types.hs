module Network.PrometheusMatrix.Types where

import           Network.PrometheusMatrix.Prelude
import           Network.PrometheusMatrix.Config

import           Servant

type AppHandler = ReaderT Config Handler
