{-# LANGUAGE DeriveGeneric    #-}
{-# OPTIONS_GHC -Wno-orphans  #-}


module Network.PrometheusMatrix.Config (
    Config(..)
  , getConfigOrDie
  , showConfig
  ) where


import           Network.PrometheusMatrix.Prelude
import           System.Envy
import qualified Network.Matrix as M


data Config = Config {
    matrixHost :: M.BaseUrl
  , matrixRoomId :: M.RoomId
  , matrixToken :: M.Token
  } deriving (Generic, Show)


instance FromEnv Config where
  fromEnv = Config <$> envMaybe "MATRIX_HOST" .!= M.defaultMatrixURI
                   <*> env      "MATRIX_ROOM_ID"
                   <*> env      "MATRIX_TOKEN"


instance Var M.BaseUrl where toVar = M.showBaseUrl; fromVar = M.parseBaseUrl

instance Var M.RoomId where toVar = show; fromVar = Just . M.RoomId . toS

instance Var M.Token where toVar = show; fromVar = Just . M.Token . toS


getConfigOrDie :: IO Config
getConfigOrDie =
  decodeEnv >>= either (die . toS) return


showConfig :: Config -> Text
showConfig = show
