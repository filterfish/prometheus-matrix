{-# LANGUAGE OverloadedStrings   #-}

module Network.PrometheusMatrix.JSONTypes (
    Annotations(..)
  , Message(..)
  , Labels(..)
  , Alert(..)
  ) where

import           Network.PrometheusMatrix.Prelude

import           Data.Aeson(Value(..), FromJSON(..), (.:), (.:?))
import           Data.Text (Text)


data Annotations = Annotations {
    description :: Maybe Text
  , summary :: Maybe Text
  } deriving (Show, Eq)


data Labels = Labels {
    alertname :: Maybe Text
  , _instance :: Maybe Text
  , job :: Maybe Text
  , severity :: Maybe Text
  } deriving (Show, Eq)


instance FromJSON Labels where
  parseJSON (Object v) = Labels <$> v .:? "alertname" <*> v .:? "instance" <*> v .:? "job" <*> v .:? "severity"
  parseJSON _          = mzero


data Alert = Alert {
    annotations :: Annotations
  , startsAt :: Text
  , labels :: Labels
  , endsAt :: Text
  } deriving (Show, Eq)


instance FromJSON Annotations where
  parseJSON (Object v) = Annotations <$> v .:? "description" <*> v .:? "summary"
  parseJSON _          = mzero


instance FromJSON Alert where
  parseJSON (Object v) = Alert <$> v .: "annotations" <*> v .: "startsAt" <*> v .: "labels" <*> v .: "endsAt"
  parseJSON _          = mzero


data Message = Message {
    status :: Text
  , alerts :: [Alert]
  , commonLabels :: Labels
  , groupKey :: Text
  , externalURL :: Text
  , commonAnnotations :: Annotations
  , receiver :: Text
  , version :: Text
  , groupLabels :: Labels
  } deriving (Show, Eq)


instance FromJSON Message where
  parseJSON (Object v) = Message <$> v .: "status" <*> v .: "alerts" <*> v .: "commonLabels"
                                 <*> v .: "groupKey" <*> v .: "externalURL"
                                 <*> v .: "commonAnnotations" <*> v .: "receiver"
                                 <*> v .: "version" <*> v .: "groupLabels"
  parseJSON _          = mzero
