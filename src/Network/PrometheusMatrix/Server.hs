{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE NoImplicitPrelude      #-}
{-# LANGUAGE PartialTypeSignatures  #-}
{-# LANGUAGE TypeOperators          #-}


module Network.PrometheusMatrix.Server (
    app
  ) where


import           Network.PrometheusMatrix.Prelude

import           Network.PrometheusMatrix.Types
import           Network.PrometheusMatrix.JSONTypes
import           Network.PrometheusMatrix.Config
import           Network.PrometheusMatrix.Handler

import           Servant


type API  = ReqBody '[JSON] Message :> Post '[JSON] NoContent


api :: Proxy API
api = Proxy


server :: Config -> Server API
server conf =
  enter (toHandler conf) alertHander


app :: Config -> Application
app conf = serve api (server conf)


toHandler :: Config -> AppHandler :~> Handler
toHandler conf = NT toHandler'
  where
    toHandler' :: forall a. AppHandler a -> Handler a
    toHandler' r = runReaderT r conf
