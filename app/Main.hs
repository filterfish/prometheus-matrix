module Main where

import           Network.PrometheusMatrix.Prelude

import           Network.PrometheusMatrix.Config
import           Network.PrometheusMatrix.Server

import           Network.Wai.Handler.Warp (Port, run)


port :: Port
port = 1329

main :: IO ()
main = do
  config <- getConfigOrDie
  hPutStrLn stderr $ showConfig config

  run port (app config)
