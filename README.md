# prometheus-matrix

[prometheus-matrix](https://gitlab.com/filterfish/prometheus-matrix) is a
simple program that implements the Prometheus Alertmanager webhook receiver
API. It is configured using enviroment variables and is intended to be run
under a daemontools like supervisor such as [nosh](http://jdebp.eu/Softwares/nosh/)
or [s6](http://www.skarnet.org/software/s6/).


## Configuration

The following enviroment variables are expected to set and exported:

* `MATRIX_HOST` — default: `https://matrix.org:8448`
* `MATRIX_ROOM_ID` — no default
* `MATRIX_TOKEN` — no default


## AlertManage Configuration

The only thing that needs to be done in the AlertManager Configuration is to set the webhook URL:

```
receivers:
- name: <...>
  webhook_configs:
  - url: http://localhost:1329/
```

## TODO

* Configurable Port number!
* Write some tests
* Properly format the alerts

## Licence

Licenced under the GPL-3 license.
